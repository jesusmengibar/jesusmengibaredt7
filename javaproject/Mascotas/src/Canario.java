
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Canario extends Aves{
    private String  color;
    private boolean canta;

    Canario() {
         super(null,0,null,null,null,false);
         this.color=null;
         this.canta=false;
         
    }

    Canario(String nom, int a, String b, String c, String pico, boolean vuela, String origen, boolean habla){
        super(nom,a,b,c,pico,vuela);
        this.color = origen;
        this.canta  = habla;
    }

    void muestra() {
       System.out.println("Nombre :"+this.getNombre());
       System.out.println("Edad   :"+this.getEdad());
       System.out.println("Estado :"+this.getEstado());
       System.out.println("Fecha Nacimiento :"+this.getFechaNac());
       System.out.println("Pico :"+this.getPico());
       System.out.println("Color :"+this.getColor());
    }
    
    void habla() {
       System.out.println("Dice Hola");
    }
    
     void saluda() {
       System.out.println("Hola hola, como estas?");
    }
     
    void volar() {
       this.setEstado("Volando");
    }
    
    void llena() throws IOException{
        BufferedReader stdin=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("");
        System.out.print("Nombre :");
        this.setNombre(stdin.readLine());
        System.out.print("Edad   :");
        this.setEdad(Integer.parseInt(stdin.readLine()));
        System.out.print("Estado :");
        this.setEstado(stdin.readLine());
        System.out.print("Fecha Nacimiento :");
        this.setFechaNac(stdin.readLine());
        System.out.print("Pico :");
        this.setPico(stdin.readLine());
        this.setVuela(false);
        System.out.print("Color :");
        this.setColor(stdin.readLine());
        this.setCanta(false);
        System.out.println("");
        System.out.println("Canario introducido con éxito");
        System.out.println("");
        
    }
    
    public String getColor() {
            return color;
    }

    public void setColor(String origen) {
            this.color = origen;
    }

    public boolean getCanta() {
            return canta;
    }

    public void setCanta(boolean habla) {
            this.canta = habla;
    }
 
}
