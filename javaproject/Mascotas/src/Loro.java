
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Loro extends Aves{
    private String  origen;
    private boolean habla;

    Loro() {
         super(null,0,null,null,null,false);
         this.origen = null;
         this.habla  = false;
    }

    Loro(String nom, int a, String b, String c, String pico, boolean vuela, String origen, boolean habla){
        super(nom,a,b,c,pico,vuela);
        this.origen = origen;
        this.habla  = habla;
    }

    void muestra() {
       System.out.println("Nombre :"+this.getNombre());
       System.out.println("Edad   :"+this.getEdad());
       System.out.println("Estado :"+this.getEstado());
       System.out.println("Fecha Nacimiento :"+this.getFechaNac());
       System.out.println("Pico :"+this.getPico());
       System.out.println("Origen :"+this.getOrigen());
    }
    
    void habla() {
       System.out.println("Dice Hola");
    }
    
    void saluda() {
       System.out.println("Hola hola, como estas?");
    }
    
    void volar() {
       this.setEstado("Volando");
    }
    
    void llena() throws IOException{
       BufferedReader stdin=new BufferedReader(new InputStreamReader(System.in));
       System.out.println("");
       System.out.print("Nombre :");
       this.setNombre(stdin.readLine());
       System.out.print("Edad   :");
       this.setEdad(Integer.parseInt(stdin.readLine()));
       System.out.print("Estado :");
       this.setEstado(stdin.readLine());
       System.out.print("Fecha Nacimiento :");
       this.setFechaNac(stdin.readLine());
       System.out.print("Raza :");
       System.out.println("");
       System.out.println("Loro introducido con éxito");
       System.out.println("");
    }
    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public boolean getHabla() {
        return habla;
    }

    public void setHabla(boolean habla) {
        this.habla = habla;
    }
 
}
