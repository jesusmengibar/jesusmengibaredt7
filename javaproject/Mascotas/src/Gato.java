
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Gato extends Mascotas{
    private String color;
    private boolean peloLargo;

    Gato() {
         super(null,0,null,null);
         this.color = null;
         this.peloLargo  = false;
    }

    Gato(String nom, int a, String b, String c, String color, boolean pelos){
        super(nom,a,b,c);
        this.color = color;
        this.peloLargo  = pelos;
    }

   void muestra() {
       System.out.println("Nombre :"+this.getNombre());
       System.out.println("Edad   :"+this.getEdad());
       System.out.println("Estado :"+this.getEstado());
       System.out.println("Fecha Nacimiento :"+this.getFechaNac());
       System.out.println("Color :"+this.getColor());
   }
    void habla() {
       System.out.println("Dice Miuau");
   }
  void llena() throws IOException{
    BufferedReader stdin=new BufferedReader(new InputStreamReader(System.in));
       System.out.println("");
       System.out.print("Nombre :");
       this.setNombre(stdin.readLine());
       System.out.print("Edad   :");
       this.setEdad(Integer.parseInt(stdin.readLine()));
       System.out.print("Estado :");
       this.setEstado(stdin.readLine());
       System.out.print("Fecha Nacimiento :");
       this.setFechaNac(stdin.readLine());
       System.out.print("Color :");
       this.setColor(stdin.readLine());
       this.setPeloLargo(false);
       System.out.println("");
       System.out.println("Gato introducido con éxito");
       System.out.println("");
   }
    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the peloLargo
     */
    public boolean getPeloLargo() {
        return peloLargo;
    }

    /**
     * @param peloLargo the peloLargo to set
     */
    public void setPeloLargo(boolean peloLargo) {
        this.peloLargo = peloLargo;
    }
}
