/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

abstract class Aves extends Mascotas{
    private String pico;
    private boolean vuela;

    Aves(String nom, int a, String b, String c, String pico, boolean vuela){
        super(nom,a,b,c);
        this.pico  = pico;
        this.vuela = vuela;
    }

    abstract void muestra();
    abstract void habla();
    abstract void volar();

    /**
     * @return the pico
     */
    public String getPico() {
        return pico;
    }

    /**
     * @param pico the pico to set
     */
    public void setPico(String pico) {
        this.pico = pico;
    }

    /**
     * @return the vuela
     */
    public boolean getVuela() {
        return vuela;
    }

    /**
     * @param vuela the vuela to set
     */
    public void setVuela(boolean vuela) {
        this.vuela = vuela;
    }
}