
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

public class Inventario {
public static void main(String args[]) throws IOException{
BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
int opcion;
String buscar;
boolean encontrado;

ArrayList <Mascotas> inventario; //utilizamos esta nueva notación para la definición de ArrayList donde se define el tipo de datos que almacenará, en este caso, Mascotas
inventario=new ArrayList<Mascotas>(); 
	

//definimos algunas mascotas de prueba:
//Perro  perro1 = new Perro("Troy",2,"Bueno","10/02/2008","Labrado",false);
//Gato   gato1  = new Gato("Misino",3,"Regular","05/05/2007","Negro",true);
//Loro   loro1  = new Loro("Pepito",10,"Muy bueno","10/05/1999","Puntiagudo",false,"Brasil",true);
//Canario  ca1  = new Canario("Amarillo",1,"Bueno","31/07/2009","corto",true,"Valencia",false);

Mascotas mascota=null, borrar = null; // no podemos instanciar la clase Mascotas por ser abstracta, pero sí podemos declarar una varible de su tipo y hacerla null;

//inventario.add(perro1);
//inventario.add(loro1);
//inventario.add(gato1);
//inventario.add(ca1);

do{
       do{
               System.out.println("");
               System.out.println(" MENU TIENDA DE MASCOTAS ");
               System.out.println();
               System.out.println(" 1: Inicializar lista mascotas");
               System.out.println(" 2: Introducir una mascota");
               System.out.println(" 3: Eliminar una mascota");
               System.out.println(" 4: Listado mascotas tienda");
               System.out.println(" 5: Listado de perros");
               System.out.println(" 6: Salir");
               System.out.println();
               System.out.print(" OPCION : ");
               opcion=Integer.parseInt(stdin.readLine());

               if(opcion<1 || opcion>6){
                   System.out.println(" OPCION INCORRECTA!!! ");
               }
       }while(opcion<1 || opcion>7);
       switch(opcion){
            case 1:
               inventario.clear();
               System.out.println("Mascotas borradas con éxito.");
               
            break;

            case 2:
               mascota=insertar();
               inventario.add(mascota);
            break;

            case 3:
                System.out.print("Mascota a buscar :");
                buscar = stdin.readLine();
                encontrado = false;
                for( Iterator <Mascotas> it2 = inventario.iterator(); it2.hasNext();) { //usamos la sintaxis nueva para q veáis cómo afecta al iterador
                    mascota = it2.next();
                    if (mascota.getNombre().equalsIgnoreCase(buscar)) {
                        borrar= mascota;
                        encontrado = true;
                    }
                }
                if (encontrado){
                    inventario.remove(borrar);
                    System.out.println("Mascota borrada con éxito.");
                }
                else
                    System.out.println("Mascota no encontrada.");

            break;

            case 4:
               if(!inventario.isEmpty()){ 
                    for( Iterator it2 = inventario.iterator(); it2.hasNext();) { //usamos la sintaxis clásica (sin definir tipo en el iterador)
                         mascota = (Mascotas) it2.next(); 
                         mascota.muestra();
                         System.out.println();
                         System.out.println();
                    }
               }else
                    System.out.println("No hay mascotas que mostrar.");
               
            break;

            case 5:
               int cont=0;
               for( Iterator it2 = inventario.iterator(); it2.hasNext();) {
                    mascota = (Mascotas) it2.next();
                    if (mascota instanceof Perro){ //ATENCIÓN a esta forma para saber el tipo de mascota!
                         mascota.muestra();
                         System.out.println();
                         cont+=1;
                    }
               }
                    if (cont==0)
                        System.out.println("No hay perros que mostrar.");

            break;

            case 6: System.out.println(" Has elegido SALIR del programa!! ");
            break;
       }
 }while(opcion!=6);
}

static Mascotas insertar()throws IOException{
    BufferedReader stdin=new BufferedReader(new InputStreamReader(System.in));
    int opcion;
    Mascotas masco1=null; 
    do{
               System.out.println(" Selecciona tipo de mascota ");
               System.out.println();
               System.out.println(" 1: Perro");
               System.out.println(" 2: Gato");
               System.out.println(" 3: Loro");
               System.out.println(" 4: Canario");
               System.out.println("");
               System.out.print(" OPCIÓN : ");
               opcion=Integer.parseInt(stdin.readLine());

               if(opcion<1 || opcion>4){
                   System.out.println(" OPCIÓN INCORRECTA!!! ");
               }
    }while(opcion<1 || opcion>4);
    
    switch(opcion){
           case 1:
               masco1 = new Perro(); //no podemos instanciar una mascota por ser una clase abstracta, pero sí una de sus clases hijas
               break;

           case 2:
               masco1 = new Gato();
               break;

           case 3:
               masco1 = new Loro();
               break;

           case 4:
               masco1 = new Canario();
               break;
        }
     masco1.llena();
     return(masco1);

 }

}
