
import java.io.IOException;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

abstract class Mascotas {
    private String nombre;
    private int edad;
    private String estado;
    private String fechaNac;
    

    public Mascotas(String nom, int a, String b, String c){
        this.nombre = nom;
        this.edad  = a;
        this.estado = b;
        this.fechaNac = c;
    }

    abstract void llena() throws IOException;
    abstract void muestra();
    abstract void habla();


    public String getNombre() {
            return nombre;
    }

    public void setNombre(String nombre) {
            this.nombre = nombre;
    }

    public int getEdad() {
            return edad;
    }

    public void setEdad(int edad) {
            this.edad = edad;
    }

    public String getEstado() {
            return estado;
    }

    public void setEstado(String estado) {
            this.estado = estado;
    }

    public String getFechaNac() {
            return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
            this.fechaNac = fechaNac;
    }
    
    public void cumpleaños() {
            this.edad = this.edad+1;
    }
     
    public void morir() {
            this.estado = "Muerto";
    }
}
