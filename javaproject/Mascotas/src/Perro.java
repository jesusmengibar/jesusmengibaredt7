
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Perro extends Mascotas{
    private String raza;
    private boolean pulgas;

    Perro() {
        super(null,0,null,null);
        this.raza = null;
        this.pulgas  = false;
    }

    Perro(String nom, int a, String b, String c, String raza, boolean pulgas){
        super(nom,a,b,c);
        this.raza = raza;
        this.pulgas  = pulgas;
    }

   void muestra() {
       System.out.println("Nombre :"+this.getNombre());
       System.out.println("Edad   :"+this.getEdad());
       System.out.println("Estado :"+this.getEstado());
       System.out.println("Fecha Nacimiento :"+this.getFechaNac());
       System.out.println("Raza :"+this.getRaza());
       System.out.println("");
       
   }
    void habla() {
       System.out.println("Dice Guau");
   }
    void llena() throws IOException{
    BufferedReader stdin=new BufferedReader(new InputStreamReader(System.in));
       System.out.println("");
       System.out.print("Nombre :");
       this.setNombre(stdin.readLine());
       System.out.print("Edad   :");
       this.setEdad(Integer.parseInt(stdin.readLine()));
       System.out.print("Estado :");
       this.setEstado(stdin.readLine());
       System.out.print("Fecha Nacimiento :");
       this.setFechaNac(stdin.readLine());
       System.out.print("Raza :");
       this.setRaza(stdin.readLine());
       this.setPulgas(false);
       System.out.println("");
       System.out.println("Perro introducido con éxito");
       System.out.println("");
   }

    /**
     * @return the raza
     */
    public String getRaza() {
        return raza;
    }

    /**
     * @param raza the raza to set
     */
    public void setRaza(String raza) {
        this.raza = raza;
    }

    /**
     * @return the pulgas
     */
    public boolean getPulgas() {
        return pulgas;
    }

    /**
     * @param pulgas the pulgas to set
     */
    public void setPulgas(boolean pulgas) {
        this.pulgas = pulgas;
    }
}
